# A notebook for RNA analysis with the Vienna RNA package

## Install

Create and activate a Conda environment:

```
conda env create --file conda-env.yml
```
(alternatively and faster, run with mamba: `mamba env create --file conda-env.yml`)


Note: conda-env.yml specifies a list of conda packages from channel
conda-forge that will be installed in the environment `vrna`.
The Vienna package library with Python bindings is installed automatically
(using pip) - this is supposed to work on common systems, including
Linux, Mac (x86 and Apple Silicon), and Windows.


## Running the examples

First, activate the conda environment vrna.

```
conda activate vrna
```

### View, edit, run the notebook

Open the Jupyter notebook in your browser by running jupyter, then
continue in the browser.

```
jupyter notebook vrna.ipynb
```

### For quick overview: display notebook results

Jupyter let's you view, edit and run the code. One can run the notebook
with voila to get only a simple overview of the demonstrated computations
and results (without Python code).

```
voila vrna.ipynb
```
